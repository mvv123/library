package org.example.services;

import org.example.dao.DictPublisherDAOImpl;
import org.example.entity.dictionary.DictPublisher;

import java.util.List;

/**
 * Сервис выполняющий CRUD-функции для сущности {@link DictPublisher}
 */
public class DictPublisherService {

    private static DictPublisherService instance;

    private DictPublisherDAOImpl publisherDAO;

    private DictPublisherService() {
        publisherDAO = new DictPublisherDAOImpl();
    }

    public DictPublisher findPublisher(Long id) {
        return publisherDAO.find(id);
    }

    public List<DictPublisher> findAllPublishers() {
        return publisherDAO.findAll();
    }

    public static DictPublisherService getInstance() {
        if(instance == null) instance = new DictPublisherService();
        return instance;
    }
}