package org.example.ui;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.example.ui.views.AuthorView;
import org.example.ui.views.BookView;
import org.example.ui.views.GenreView;

public class CustomizeVerticalLayout extends VerticalLayout {

    public CustomizeVerticalLayout() {
        setMargin(true);
    }

    protected void createMenu() {
        MenuBar menu = new MenuBar();

        menu.addItem(AuthorView.NAME , menuItem -> UI.getCurrent().getNavigator().navigateTo(AuthorView.URL));

        menu.addItem(BookView.NAME , menuItem -> UI.getCurrent().getNavigator().navigateTo(BookView.URL));

        menu.addItem(GenreView.NAME , menuItem -> UI.getCurrent().getNavigator().navigateTo(GenreView.URL));

        addComponent(menu);
    }
}