# Example code and test project #

This project was created to showcase my code and technology skills.

### Technology ###

- Java SE
- Vaadin
- Hibernate
- Maven
- HSQLDB

### Build and Run ###

1. Run in the command line:
	```
	mvn package
	mvn jetty:run
	```

2. Open `http://localhost:8080` in a web browser.
